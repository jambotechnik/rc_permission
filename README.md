# RC_Permission
## _Legamaster - MobiControl Remote Control_

Diese App setzt die Berechtigungen für das Remote Control von MobiControl auf
Legamaster Boards.

Zunächst muss die App einmalig gestartet werden und mit Apply die Berechtigung gesetzt werden.
Anschließend startet die App, sobald das Board hochgefahren wird und setzt automatisch die Remote Control Berechtigung.

## Testen
Um die App im Emulator zu testen muss im _CommandExecutor_ der Socket Host auf "10.0.2.2" anstelle von "127.0.0.1" gesetzt werden.

## Library
Für das absetzen des _adb_-Befehls wurde folgende Dependency verwendet:
https://github.com/tananaev/adblib