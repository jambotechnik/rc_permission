package de.jambo.rc_permission;

import android.content.Context;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;

import com.tananaev.adblib.AdbBase64;
import com.tananaev.adblib.AdbConnection;
import com.tananaev.adblib.AdbCrypto;
import com.tananaev.adblib.AdbStream;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Execute the adb shell command.
 */
public class CommandExecutor {

    public CommandExecutor() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static AdbBase64 getBase64() {
        return data -> Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public static AdbCrypto setupCrypto(String publicKeyFile, String privateKeyFile) throws
            NoSuchAlgorithmException, IOException {
        File publicFile = new File(publicKeyFile);
        File privateFile = new File(privateKeyFile);
        AdbCrypto crypto = null;

        if (publicFile.exists() && privateFile.exists()) {
            try {
                crypto = AdbCrypto.loadAdbKeyPair(CommandExecutor.getBase64(), privateFile, publicFile);
            } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                crypto = null;
            }
        }

        if (crypto == null) {
            crypto = AdbCrypto.generateAdbKeyPair(CommandExecutor.getBase64());
            crypto.saveAdbKeyPair(privateFile, publicFile);
        }

        return crypto;
    }

    /**
     * Command to enable MobiControl remote control.
     */
    private final String COMMAND = "settings put secure enabled_accessibility_services net.soti.mobicontrol.androidwork/net.soti.mobicontrol.appops.accessibilityservice.AfwMobiControlAccessibilityService";

    public void execute(Context context) throws Exception {
        Log.i("EXECUTE", "start...");
        AdbConnection connection;
        Socket socket;
        AdbCrypto crypto;

        try {
            String path = context.getFilesDir().getPath();
            crypto = setupCrypto(path + "/pub.key", path + "/priv.key");
        } catch (NoSuchAlgorithmException | IOException e) {
            Log.e("ERR", e.getMessage());
            throw new Exception(e.getMessage());
        }

        Log.i("EXECUTE", "Socket connecting...");
        try {
            //socket = new Socket("10.0.2.2", 5555); // android emulator
            socket = new Socket("127.0.0.1", 5555);
        } catch (IOException e) {
            Log.e("ERR", e.getMessage());
            throw new Exception(e.getMessage());
        }
        Log.i("EXECUTE", "Socket connected.");

        try {
            connection = AdbConnection.create(socket, crypto);
        } catch (IOException e) {
            Log.e("ERR", e.getMessage());
            throw new Exception(e.getMessage());
        }

        Log.i("EXECUTE", "ADB connecting...");
        try {
            connection.connect();
        } catch (IOException | InterruptedException e) {
            Log.e("ERR", e.getMessage());
            throw new Exception(e.getMessage());
        }

        Log.i("EXECUTE", "ADB connected.");
        final AdbStream stream;
        try {
            stream = connection.open("shell:");
        } catch (IOException | InterruptedException e) {
            Log.e("ERR", e.getMessage());
            throw new Exception(e.getMessage());
        }

        new Thread(() -> {
            while(!stream.isClosed()) {
                try {
                    Log.i("EXECUTE", new String(stream.read(), "UTF-8"));
                } catch (IOException | InterruptedException e) {
                    Log.e("ERR", e.getMessage());
                    return;
                }
            }
        }).start();

        try {
            stream.write(COMMAND+'\n');
        } catch (IOException | InterruptedException e) {
            Log.e("ERR", e.getMessage());
            return;
        }
    }

}
