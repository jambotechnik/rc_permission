package de.jambo.rc_permission;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

/*
 * Main Activity.
 */
public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        boolean isApplied = prefs.getBoolean("isApplied", false);
        if (isApplied) {
            executeCommand(this);
            this.finishAffinity();
        }

        findViewById(R.id.cancelBtn).setOnClickListener(v -> this.finishAffinity());
        findViewById(R.id.okBtn).setOnClickListener(this::apply);
    }

    public void apply(View v) {
        executeCommand(v.getContext());
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isApplied", true);
        editor.apply();

        this.finishAffinity();
    }

    private void executeCommand(Context context) {
        CommandExecutor commandExecutor = new CommandExecutor();
        try {
            commandExecutor.execute(getApplicationContext());
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

}